<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9ef50fc8-7741-49ea-9716-9e72627c2c4e</testSuiteGuid>
   <testCaseLink>
      <guid>032e6efc-197f-4857-bc15-76856807952b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create user/TC Create user - success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd77a5d8-fc24-498c-ac18-56f3d8663b16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create user/TC Create user with wrong endpoint</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6087f23e-c10b-42c8-96c8-95ef8ca5e88e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3580f99b-0955-4752-8143-01758773f916</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/TC Register user - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd4fe68a-d270-46df-83b1-084b018617f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login without password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7b3f462-574a-42fb-91c8-24f87d1976cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login without email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c9b8c9-0856-4cf4-88d1-b0660f535d9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register/TC Register without password</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
